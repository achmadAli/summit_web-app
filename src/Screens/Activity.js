import React, {Component} from 'react';
import AppBar from '../components/AppBar';
import User from '../User';
import CardActivity from '../components/CardActivity';
import { request } from 'https';
// import emptyActivity from '../assets/images/empty_activity.png';

class Activity extends Component {
  constructor(props){
    super(props);

    this.state = {
      activities : [],

    }
  }

  componentDidMount(){

    const request = require('request');
    
    const option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     'http://192.168.43.129:3000/activities',
    }

    request(option, (error, {body, statusCode}) => {
      console.log(error);
      
      if(statusCode === 200){
        this.setState({
          activities: JSON.parse(body)
        })
        console.log(JSON.parse(body));
      }
    })
  }

  render(){
    return ( 
       <React.Fragment>
        <AppBar title="Activity" {...this.props}/>
        <div className="container">
        {this.state.activities.map(data => <CardActivity {...this.props} activity={data} />)}
        </div>
       </React.Fragment>
     );
  }
}
 
export default Activity;
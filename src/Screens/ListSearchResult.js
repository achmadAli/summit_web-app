import React, {Component} from 'react';


class ListSearchResult extends Component {
  constructor(props){
    super(props);

    this.setState = {
      events : [],
      guides : []
    }
  }

  componentDidMount(){
    const request = require('request');
    let endppoint = this.props.location.pathname.split('/')[1];
    
    request(`http://192.168.43.129:3000/${endppoint}/mountain/${this.props.match.params.mountainId}`, (error, {body, statusCode}) => {
      if(statusCode === 200 && !error){
        this.setState({
          [endppoint+'s'] : JSON.parse(body)
        })
      }
    })
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
        </div>
      </React.Fragment>
    )
  }
}

export default ListSearchResult;
import React from 'react';
import {Link} from 'react-router-dom';
import Search from '../components/Search';
import CardMountain from '../components/CardMountain';
import eventImg from '../assets/images/event_art.png';
import guideImg from '../assets/images/guide_art.png';

const SearchResult = (props) => {  

  const destination = props.history.location.state;

  function getMountainName(){
    if(destination.name.toLowerCase().search('gunung') > -1 )
      return destination.name.replace("Gunung ", "");
    else if(destination.name.toLowerCase().search('gn') > -1)
      return destination.name.replace("gn ", "");
  }

  return(
    <React.Fragment>
      
      <div className="container mt-3 pt-0">
        <Search {...props}/>
        <h2> Gunung {getMountainName()} </h2>
        <div className="card">

          <CardMountain mountain={destination} />

          <Link to={`/event/mountain/${destination._id}`} className="card__item">
            <img 
              src={eventImg} 
              alt="eventImage"
              className="card__image"/>
            <div className="card__item__detail">
              <h3> Event </h3>
              <p>Temukan event pendakian seputar Gunung Semeru yang cocok untukmu!</p>
            </div>
          </Link>

          <div className="card__item">
            <img 
              src={guideImg} 
              alt="eventImage"
              className="card__image"/>
            <div className="card__item__detail">
              <h3> Meet your Guide ! </h3>
              <p> Cari mountain-guide yang pas untuk perjalanan pendakianmu di Semeru! </p>
            </div>
          </div>
  
          
        </div>

      </div>
    </React.Fragment>
  );
}

export default SearchResult;
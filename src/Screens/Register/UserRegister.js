import React from 'react';
import CommonFormRegister from './CommonFormRegister';

const UserRegister = ()=> {
  return(
    <div className="container">
      <h1 className="mt-5 mb-4"> Daftar </h1>
      <form action="" className="login__form-wraper">
        <CommonFormRegister />
        <small> Dengan mendaftarkan diri, Anda menyetujui <strong> Syarat Ketentuan dan Privasi. </strong> </small>
        <button className="btn btn__primary btn--block mt-3">
          Daftar
        </button>
      </form>
    </div>
  );
}

export default UserRegister;
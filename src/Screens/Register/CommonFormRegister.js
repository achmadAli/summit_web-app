import React from 'react'


const CommonFormRegister = () => {
  return(
    <React.Fragment>
      <div className="form__group-input">
        <label htmlFor="name"> Nama </label>
        <input 
          type="text" 
          name="name" 
          id="name"
          className="form__input"
          placeholder="E.g John Doe"/>
      </div>
     
      <div className="form__group-input">
        <label htmlFor="address"> Alamat </label>
        <input 
          type="text" 
          name="address" 
          id="address"
          className="form__input"
          placeholder="E.g Malang, East java"/>
      </div>
     
      <div className="form__group-input">
        <label htmlFor="phone"> Nomor Telepon </label>
        <input 
          type="text" 
          name="phone" 
          id="phone"
          className="form__input"
          placeholder="E.g 08XXXXXX111"/>
      </div>
     
      <div className="form__group-input">
        <label htmlFor="email"> Email </label>
        <input 
          type="email" 
          name="email" 
          id="email"
          className="form__input"
          placeholder="E.g JohnDoe@example.com"/>
      </div>
     
      <div className="form__group-input">
        <label htmlFor="username"> Username </label>
        <input 
          type="username" 
          name="username" 
          id="username"
          className="form__input"
          placeholder="E.g John_Doe"/>
      </div>
     
      <div className="form__group-input">
        <label htmlFor="password"> Password </label>
        <input 
          type="password" 
          name="password" 
          id="password"
          className="form__input"
          placeholder="E.g *****"/>
      </div>
     
      <div className="form__group-input">
        <label htmlFor="confirmPassword"> Konfirmasi Password </label>
        <input 
          type="password" 
          name="confirmPassword" 
          id="confirmPassword"
          className="form__input"
          placeholder="E.g *****"/>
      </div>


    </React.Fragment>
  );
}

export default CommonFormRegister;
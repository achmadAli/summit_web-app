import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import registerArt from '../assets/images/register_art.svg';


class Register extends Component{

  render(){
    return(
      <div className="container register register--bg-primary">
        <img 
          src={registerArt}
          className="img-responsive my-5"
          alt="register"/>

        <Link to="/userRegister" className="btn register__btn"> USER </Link>
        <Link to="/GuideRegister" className="btn register__btn"> GUIDE </Link>
      </div>
    );
  }
}

export default Register;
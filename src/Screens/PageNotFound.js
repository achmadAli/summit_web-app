import React from 'react';
import pageNotFoundImage from '../assets/images/not_found.png';

const PageNotFound = () => {
  return ( 
    <React.Fragment>
      <img 
        src={pageNotFoundImage}
        className="img--responsive"
        alt="page not found"/>
    </React.Fragment>
   );
}
 
export default PageNotFound;
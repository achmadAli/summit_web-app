import React, { Component } from 'react';
import Search from '../components/Search';
import CardMountain from '../components/CardMountain';
import CardEvent from '../components/CardEvent';
import CardGuide from '../components/CardGuide';

export default class Home extends Component { 

  constructor(props){
    super(props);
    this.state = {
      events      : localStorage.getItem('events') != null ? JSON.parse(localStorage.getItem('events')): [],
      mountains   : localStorage.getItem('mountains') != null ? JSON.parse(localStorage.getItem('mountains')): [],
      guides      : localStorage.getItem('guides') != null ? JSON.parse(localStorage.getItem('guides')): [],
    }
  }

  componentDidMount(){
    const request = require('request');
    const endpoint = (endpoint) => {
      return `http://192.168.43.129:3000/${endpoint}`
    }

    if(localStorage.getItem("mountains") == null) { 
      request(endpoint('mountains'), (error, {body, statusCode}) => {
        if(statusCode === 200 && !error){
          if(this.state.mountains.length === 0){
            this.setState({
              mountains : JSON.parse(body)
            })
  
            localStorage.setItem('mountains', body);
          }
        }
      });
  
      request(endpoint('events'), (error, {body, statusCode}) => {
        if(statusCode === 200 && !error){
          this.setState({
            events : JSON.parse(body)
          })
  
          localStorage.setItem('events', body);
        }
      });
  
      request(endpoint('guides'), (error, {body, statusCode}) => {
        if(statusCode === 200 && !error){
          this.setState({
            guides : JSON.parse(body)
          })
  
          localStorage.setItem('guides', body);
        }
      });

    }
  }
  
  render() { 
    return(
      <div className="container mt-3 pt-0">
        <Search {...this.props} />
        
        <h3 className="mt-3"> Gunung Terpopuler </h3>
        <div className="card card--horizontal-scroll">
          {this.state.mountains.map(data => <CardMountain key={data._id} mountain={data} {...this.props} />)}
        </div>

        <h3 className="mt-2"> Event Terbaru </h3>
        <div className="card">
          {this.state.events.map(data => <CardEvent key={data._id} event={data}  />)}
        </div>

        <h3 className="mt-2"> Guide Terpopuler </h3>
        <div className="card">
          {this.state.guides.map( data => <CardGuide key={data._id} guide={data} />)}
        </div>
      </div>
    );
  }
}
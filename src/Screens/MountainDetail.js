import React from 'react';
import AppBar from '../components/AppBar';


const MountainDetail = (props) => {
  const {mountain} = props.location.state;
  return(
    <React.Fragment>
      <AppBar title={mountain.name} {...props} />

      <img 
        src={mountain.coverImgPath}
        alt={props.name}
        className="img-responsive mb-4"/>

      <div className="container" dangerouslySetInnerHTML={{ __html: mountain.content }}>
      </div>
    </React.Fragment>
  );
}

export default MountainDetail;

import React from 'react';
import {Link} from 'react-router-dom';
import loginArt from '../assets/images/loginArt.png';

const LoginRequired = () => {
  return(
    <div className="container display-flex justify-content-center vh-100" style={{flexFlow: "column"}}>
      <img 
      src={loginArt}
      className="img-responsive"
      alt="art login" />

      <Link to="/login" className="btn btn__primary mt-5"> Login </Link>
    </div>
  );
}

export default LoginRequired;
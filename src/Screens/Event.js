import React, {Component} from 'react';
import AppBar from '../components/AppBar';
import AddEvent from '../components/FloatBtn';
import CardEvent from '../components/CardEvent';
import emptyActivity from '../assets/images/empty_activity.png';
import User from '../User';

class Event extends Component {
  constructor(props){
    super(props);

    this.state = {
      events : []
    }
  }

  componentDidMount(){
    const request = require('request');
    const option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     'http://192.168.43.129:3000/event/me',
    }

    request(option, (error, {body, statusCode}) => {
      
      if(statusCode === 200 && !error){
        let fetchResult = JSON.parse(body);
        
        console.log(JSON.parse(body));
        if(fetchResult.length > 0){
          this.setState({
            events : fetchResult.map( data => <CardEvent key={data._id} event={data} {...this.props} /> )
          })
        } else {
          this.setState({
            events : <img src={emptyActivity} className="img-responsive" alt="empty"/>
          })
        }
      }
    })
  }
  
  
  render() {
    console.log(this.props);
    return (
      <React.Fragment>
        <AppBar title="My Event" {...this.props}/>

        <div className="container">
          {this.state.events}
        </div>

        <AddEvent {...this.props}/>
      </React.Fragment>
    );
  }
}
 
export default Event;
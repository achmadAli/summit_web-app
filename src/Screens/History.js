import React, {Component} from 'react';
import User from '../User';
import AppBar from '../components/AppBar';
import CardEvent from '../components/CardEvent';

class History extends Component{
  constructor(props){
    super(props);

    this.state = {
      events: null
    }
  }

  componentDidMount(){
    const request = require('request');
    const option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     'http://192.168.43.129:3000/event/me',
    }

    request(option, (error, {body, statusCode}) => {
      console.log(body);
      
      if(statusCode === 200 && !error){
        let fetchResult = JSON.parse(body);

        if(fetchResult.length > 0){
          this.setState({
            events : fetchResult.map( data => <CardEvent key={data._id} event={data} {...this.props} /> )
          })
        }
      }
    })
  }

  render() {
    return (
      <React.Fragment>
        <AppBar title="History Activity" {...this.props} />
        <div className="container">
          {this.state.events}
        </div>
      </React.Fragment>
    );
  }
}

export default History;
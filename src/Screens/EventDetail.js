import React from 'react';
import AppBar from '../components/AppBar';
import Helper from '../helpers/helper';
import eventImage from '../assets/images/event_art.png';
import leaderImage from '../assets/images/dummyPhoto.jpg';
import User from '../User';

const EventDetail = (props) => {
  const {event} = props.location.state;

  let leader = {};

  require('request')(`http://192.168.43.129:3000/user/${event.leader}`, (error, {body, statusCode}) => {
    if(statusCode === 200){
      leader = JSON.parse(body);
    }
  })

  function requestJoin(){
    const request = require('request');
    let option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     `http://192.168.43.129:3000/event/join/${event._id}`,
      body:     JSON.stringify(this.state.event)
    }

    request.post(option, (error, {body, statusCode}) => {
      if(statusCode === 201){
        this.props.history.push('/event');
      }
    })    
  }

  return(
    <React.Fragment>
      <AppBar title={event.title} {...props} />
      <div className="container event">
        <img 
          src={eventImage} 
          alt="eventImage"
          className="event__hero"/>

          <div className="event__destination">
            <h3> {Helper.transformIdToMountainName(event.mountain)} </h3>
            <table>
              <tbody>
                <tr>
                  <td> Berangkat </td>
                  <td>  : {Helper.formatDate(event.startAt)} </td>
                </tr>
                <tr>
                  <td> Selesai </td>
                  <td> : {Helper.formatDate(event.finishAt)} </td>
                </tr>
                <tr>
                  <td> Anggota</td>
                  <td> : {event.members.length} / {event.maximumMembers}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className="event__leader">
            <h3> Leader</h3>

            <div className="event__leader__information">
              <img 
                src={leaderImage} 
                alt="leaderImage"
                className="event__leader__image"/>

              <div className="ml-3">
                <h4> {console.log(leader)}</h4>
                <p> 20 tahun </p>
                <p> Sidoarjo </p>
              </div>
            </div>

          </div>

          <h3 className="mt-5 mb-2"> Informasi Event </h3>
          <p> {event.content} </p>

          <button className={`btn btn--block btn--primary mt-4 ${Helper.eventAvailability(event._id)}`} onClick={requestJoin}> Join Event </button>
      </div>
    </React.Fragment>
  );
}

export default EventDetail;
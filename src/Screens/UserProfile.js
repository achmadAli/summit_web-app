import React from 'react'
import AppBar from '../components/AppBar';
import User from '../User';
import Helper from '../helpers/helper';
import Experience from '../components/Experience';
import dummyPhoto from '../assets/images/dummy_potrait_photo.png';
import dummyBadge from '../assets/images/badges/mountain.svg';

const UserProfile = (props)=>{
  
  let user              = props.location.state !== undefined ? props.location.state.guide : User;
  let requestElement    = props.match.params.idUser !== undefined ? (<div className="request"> <button className="btn btn--primary btn--block"> Request Guide </button> </div>) : "";

  const experienceData = [
    {
      image   : dummyBadge,
      experience : 3,
      mountain: "Gunung Sumeru",
      year    : "2017" 
    },
    {
      image   : dummyBadge,
      experience : 4,
      mountain: "Gunung Sumeru",
      year    : "2017" 
    },
    {
      image   : dummyBadge,
      experience : 1,
      mountain: "Gunung Sumeru",
      year    : "2017" 
    },
    {
      image   : dummyBadge,
      experience : 2,
      mountain: "Gunung Sumeru",
      year    : "2017" 
    },
    {
      image   : dummyBadge,
      experience : 5,
      mountain: "Gunung Sumeru",
      year    : "2017" 
    },
  ];

  return(
    <React.Fragment>
      <AppBar title="Profile User" {...props} />

      <div className="container pb-0">
        <div className="user-profile">
          <div className="user-profile__bio">
            <h2> {user.getName()} </h2>
            <h4 className="mb-0"> 20 Tahun </h4>
            <h5> {user.getAddress()} </h5>
            <h5> {Helper.hidePhoneDetail("08998496687")} </h5>
          </div>
          <img 
            src={dummyPhoto} 
            alt="user_photo"
            className="user-profile__photo"/>
        </div>
      </div>

      <div className="user-experience">
        <h2 className="text-center my-3"> Pengalaman Pendakian </h2>
        <div className="user-experience__badges">
          {experienceData.map( data => 
            <Experience 
              image={Helper.setBadgeByExperience(data.experience)}
              mountain={data.mountain}
              year={data.year} />
          )}
        </div>
      </div>
      {requestElement}
    </React.Fragment>
  );
}

export default UserProfile;
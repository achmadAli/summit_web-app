import React from 'react';
import maintenanceImage from '../assets/images/desktopOnMaintenance.svg';


const MaintenanceDesktop = () => {
  return ( 
    <div className="container mt-5 pt-5">
      <img 
        src={maintenanceImage} 
        alt="maintenance art"
        className="img-responsive display-block mx-auto w-50"/>
      <h2 className="text-center mt-5"> Comming Soon </h2>
      <h2 className="text-center"> Application for Desktop  </h2>
    </div>
   );
}
 
export default MaintenanceDesktop;
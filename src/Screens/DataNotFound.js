import React from 'react';
import noResultImage from '../assets/images/noResult.svg';

const DataNotFound = (props) => {

  function back(){
    props.history.goBack();
  }
  
  return(
    <React.Fragment>
      <div className="container mt-5">
        <img 
          src={noResultImage}
          className="img-responsive mt-3"
          style={{padding: "60px"}}
          alt="no result"/>

        <button className="btn btn--primary display-block mx-auto mt-4" onClick={back}> Kembali </button>
      </div>
    </React.Fragment>
  );
}

export default DataNotFound;

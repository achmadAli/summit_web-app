import React, {Component} from 'react';
import AppBar from '../components/AppBar';
import User from '../User';
// import User from '../User';


class CreateEvent extends Component {
  constructor(props){
    super(props);
    this.state = {
      event : {
        title           : "",
        type            : 1,
        price           : 0,
        startAt         : "2019-10-14",
        finishAt        : "2019-10-18",
        maximumMembers  : 0,
        mountain        : ""
        },
      mountains : []
    }

    this.submitHandler = this.submitHandler.bind(this);
    this.formHandler = this.formHandler.bind(this);
  }

  componentDidMount(){
    const request = require('request');

    request('http://192.168.43.129:3000/mountains', (error, {body ,statusCode}) => {
      if(statusCode === 200 && !error){
        this.setState({
          mountains : JSON.parse(body)
        })
      }
    });
  }

  requestGuide(event_id, guideId){
    const request = require('request');
    
    const option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`,
        'body'          : JSON.stringify({
          "event" : event_id
        })
      },
      uri:     `http://192.168.43.129:3000/guide/request/${guideId}`,
    }

    request.post(option, (error, {body, statusCode}) => {      
      console.log(statusCode);
      if(statusCode === 200){
        this.props.history.push('/activities');
      }
    })
  }

  formHandler(event){
    let {name, value} = event.target;
    
    this.setState({
      event: {
        ...this.state.event,
        [name] : value
      }
    })
    
  }

  submitHandler(event){
    event.preventDefault();
    const request = require('request');

    let option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     'http://192.168.43.129:3000/event',
      body:     JSON.stringify(this.state.event)
    }

    request.post(option, (error, {body, statusCode}) => {
      if(statusCode === 201){
        if(this.props.location.state.guideId !== null || this.props.location.state.guideId !==undefined){
          this.requestGuide(body._id, this.props.guideId);
        }
        this.props.history.push('/event');
      }
      
    })
  }

  priceVisibility(){
    if(this.state.event.type === 1){
      return "display-none"
    }

    return '';
  }

  render(){
    return(
      <React.Fragment>
        <AppBar title="Create Event" {...this.props} />
        <div className="container">
          <form className="form" onSubmit={this.submitHandler}>
            <div className="form__group-input">
              <label htmlFor="title"> Nama Event </label>
              <input 
                type="text" 
                name="title" 
                id="title"
                className="form__input"
                placeholder="E.g Pendakian Arjuno"
                onChange={this.formHandler}/>
            </div>

            <div className="form__group-input">
              <label htmlFor="startAt"> Berangkat </label>
              <input 
                type="date" 
                name="startAt" 
                id="startAt"
                className="form__input"
                placeholder={this.state.event.startAt}
                onChange={this.formHandler}/>
            </div>

            <div className="form__group-input">
              <label htmlFor="finishAt"> Selesai </label>
              <input 
                type="date" 
                name="finishAt" 
                id="finishAt"
                className="form__input"
                placeholder={this.state.event.finishAt}
                onChange={this.formHandler}/>
            </div>

            <div className="form__group-input">
              <label htmlFor="maximumMembers"> Jumlah anggota </label>
              <input 
                type="number" 
                name="maximumMembers" 
                id="maximumMembers"
                className="form__input"
                placeholder={this.state.event.maximumMembers}
                onChange={this.formHandler}/>
            </div>

            <div className="form__group-input">
              <label htmlFor="mountain"> Tujuan Pendakian </label>
              <select name="mountain" className="form__input" onChange={this.formHandler} >
                {this.state.mountains.map( ({_id, name}) => <option key={_id} value={_id}> {name} </option>)}
              </select>
            </div>

            <div className="form__group-input">
              <label htmlFor="type"> Jenis Event </label>
              <select name="type" className="form__input" onChange={this.formHandler} >
                <option value={1}> Gratis </option>
                <option value={2}> Berbayar </option>
                <option value={3}> Full Service </option>
              </select>
            </div>

            <div className={`form__group-input ${this.priceVisibility()}`}>
              <label htmlFor="price"> Harga </label>
              <input 
                type="number" 
                name="price" 
                id="price"
                className="form__input"
                placeholder={this.state.event.price}
                onChange={this.formHandler}/>
            </div>

            <div className={`form__group-input`}>
              <label htmlFor="content"> Keterangan </label>
              <textarea 
                type="text" 
                name="content" 
                id="content"
                rows={10}
                height={10}
                className="form__input"
                placeholder={this.state.event.content}
                onChange={this.formHandler}/>
            </div>

            <button type="submit" className="btn btn--primary btn--block mt-5"> Buat Event </button>
          </form>
        </div>
      </React.Fragment>
    );
  }
}


export default CreateEvent;
import React, { Component } from 'react';
import artImage from '../assets/images/new_users_art.png';

class StartHiking extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      destination: ""
     }

     this.formHandler   = this.formHandler.bind(this);
     this.submitHandler = this.submitHandler.bind(this);
  }

  formHandler(event){
    this.setState({
      destination: event.target.value
    })
  }

  submitHandler(event){
    event.preventDefault();

    const request = require('request');
    let option = {
      headers: {'content-type' : 'application/json'},
      uri:     `http://192.168.43.129:3000/mountain?q=Gunung ${this.state.destination}`
    }
    
    request(option, (error, {body, statusCode}) => {
      if(statusCode === 200 && !error){
        let bodyObj     = JSON.parse(body);

        this.props.history.push(`/result/${bodyObj._id}`, { destination : bodyObj} );
      }
    })
  }


  render(){
    
    return(
      <div className="start-hiking">
        <div className="container">
          <h2 className="text-center"> Halo Hikers! </h2>
          <img
            src={artImage}
            className="img-responsive px-5"
            alt="art for new users"
            />
            <h3 className="text-center mt-5"> Mau ke pergi ke gunung mana ?</h3>
            <form className="start-hiking__form" onSubmit={this.submitHandler}>
              <input 
                type="text" 
                name="searchLocation" 
                placeholder="E.g Gunung Rinjani" 
                onChange={this.formHandler}
                className="form__input form__input--center form__input--rounded" 
                />
              <button type="submit" className="btn btn__primary my-3 mx-auto"> Submit </button>
            </form>
        </div>
      </div>
    );
  }
}

export default StartHiking;
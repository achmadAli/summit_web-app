import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import User from '../User';
import trackArt from '../assets/images/artForLogin.svg';


class Login extends Component{
  constructor(props){
    super(props);

    this.state = {
      auth : {
        email: "",
        password: ""
      }
    }

    this.formHandler = this.formHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  formHandler(element){
    this.setState({
      auth : {
        ...this.state.auth,
        [element.target.name] : element.target.value
      }
    });
  }

  submitHandler(event){
    event.preventDefault();

    const request = require('request');
    let option = {
      headers: {'content-type' : 'application/json'},
      uri:     'http://192.168.43.129:3000/login',
      body:     JSON.stringify(this.state.auth)
    }

    request.post(option, (error, {body, statusCode}) => {
      console.log(body);
      
      if(statusCode === 200 && !error){
        let {user, token} = JSON.parse(body); 
        
        User.setUser(user);
        User.setToken(token);

        setTimeout(()=>{
          this.props.history.push('/');
        }, 1000)

      } else {
        alert("Login Failed")
      }
    })
    
  }
  
  render(){
    return(
      <div className="container login">
        <div className="login-header">
          <img 
            src={trackArt} 
            alt="Track Art"
            className="login__image--absolute"/>
          <h1 className="login__header"> Login </h1>
        </div>

        <form className="login__form-wrapper" onSubmit={this.submitHandler}>
          <div className="form__group-input">
            <label htmlFor="email"> Email </label>
            <input 
              type="text" 
              name="email" 
              id="email"
              className="form__input form__input--large"
              placeholder="E.g John Doe"
              onChange={this.formHandler}/>
          </div>
          <div className="form__group-input mt-4">
            <label htmlFor="password"> Password </label>
            <input 
              type="password" 
              name="password" 
              id="password"
              className="form__input form__input--large"
              placeholder="Password"
              onChange={this.formHandler}/>
          </div>
          <Link to="/forgot" className="text-primary" > Lupa Password anda ? </Link>

          <button type="submit" className="btn btn--primary btn--block mt-5"> Login </button>
        </form>
        
        <div className="text-center mt-auto mb-3">
          <p className="my-0" onClick={this.submitHandler}> Belum Punya Akun ?</p>
          <Link to="/register" className="text-primary"> Daftar disini</Link>
        </div>
      </div>
    );
  }
}

export default Login;
import starter from '../assets/images/badges/starter-hiker.svg';
import middle from '../assets/images/badges/middle-hiker.svg';
import expert from '../assets/images/badges/expert-hiker.svg';
import User from '../User';

class Helpers{

  formatDate(date){
    const months = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agust", "Sep", "Okt", "Nov", "Des"];
    date = date.substr(0, 10).split('-');

    return `${date[2]} ${months[date[1] - 1]} ${date[0]}`;
  }

  transformIdToName(id, callBack){
    const request = require('request');
    request(`http://192.168.43.129:3000/user/${id}`, (error, {body, statusCode}) => {
      if(statusCode === 200){
        callBack(JSON.parse(body).name)
      }
    })
  }

  
  transformIdToMountainName(id){
    require('request')(`http://192.168.43.129:3000/mountain/${id}`, (error, {body, statusCode}) => {
      if(statusCode === 200){
        return JSON.parse(body).name;
      }
    })
  }

  hidePhoneDetail(number){
    let middleNumber = number.substr(2, 7);
    return number.replace(middleNumber, "XXXXXXX");
  }

  setBadgeByExperience(count){
    if(count === 1)       return starter;
    else if (count <= 3)  return middle;
    else                  return expert;
  }

  eventAvailability(id){
    const request = require('request');
    let option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     `http://192.168.43.129:3000/event/me`
    }

    request.post(option, (error, {body, statusCode}) => {
      if(statusCode === 200){
        let IdEvents = [];
        JSON.parse(body).forEach( data => {
          IdEvents.push(data._id);
        })

        if(IdEvents.indexOf(id) >= 0){
          return "display-none";
        }

        return '';
      }
    })    
  }

  changeEventType(type){
    switch (type) {
      case 1:
        return "Gratis";
      case 2:
        return "Berbayar";
      case 3:
        return "Full Service"
      default:
        break;
    }
  }

}


export default new Helpers();
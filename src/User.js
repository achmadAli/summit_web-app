class User{
  constructor(){
    this.id = "";
    this.name = "";
    this.email = "";
    this.identityNumber = "";
    this.birthDate = "";
    this.phoneNumber = "";
    this.address = "";
    this.role = 0;
    this.experiences = [];
    this.token = "";
    this.authenticated = false;


    const userObj = JSON.parse(localStorage.getItem("user"));
    if( userObj !== null){
      this.setUser(userObj);
      this.setToken(localStorage.getItem("token"))
    } else {
    }
  }

  
  setUser(user){
    this.id = user._id;
    this.name = user.name;
    this.email = user.email;
    this.identityNumber = user.identityNumber;
    this.birthDate = user.birthDate;
    this.phoneNumber = user.phoneNumber;
    this.address = user.address;
    this.role = user.role;
    this.experiences = user.experiences;
    localStorage.setItem("user",JSON.stringify(user))
    this.setAuth();
  }
  
  setToken(token){
    this.token = token;
    localStorage.setItem("token", token);
  }

  getToken(){
    return this.token;
  }

  setAuth(){
    this.authenticated = !this.authenticated;
  }

  isAuthenticated(){
    return this.authenticated;
  }

  getId(){
    return this.id;
  }
  
  getName(){
    return this.name;
  }

  getAddress(){
    return this.address;
  }
  
  getPhoneNumber(){
    return this.phoneNumber;
  }
}

export default new User();
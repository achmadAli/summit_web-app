import React from 'react';
import {NavLink} from 'react-router-dom';

const Navigation = () => {
  return(
    <nav className="navigation">
        <NavLink exact to="/" className="navigation__home" activeClassName="navigation__home--active" /> 
        <NavLink to="/activity" className="navigation__activity" activeClassName="navigation__activity--active" /> 
        <NavLink to="/event" className="navigation__event" activeClassName="navigation__event--active" /> 
        <NavLink to="/profile" className="navigation__profile" activeClassName="navigation__profile--active" /> 
    </nav>
  )
}

export default Navigation;
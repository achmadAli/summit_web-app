import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import Navigation   from './Navigation';

import Activity       from './Screens/Activity';
import CreateEvent    from './Screens/CreateEvent';
import DataNotFound   from './Screens/DataNotFound';
import EventDetail    from './Screens/EventDetail';
import Event          from './Screens/Event';
import Home           from './Screens/Home';
import History        from './Screens/History';
import Login          from './Screens/Login';
import ListResult     from './Screens/ListSearchResult';
import MountainDetail from './Screens/MountainDetail';
import PageNotFound   from './Screens/PageNotFound';
import SearchResult   from './Screens/SearchResult';
import StartHiking    from './Screens/StartHiking';
import UserProfile    from './Screens/UserProfile';
import GuideProfile    from './Screens/GuideProfile';

class App extends Component{
  render() {
    return(
      <React.Fragment>
        <Router>
          <Navigation />
          
          <Switch> 
            <Route path="/start" component={StartHiking} />
            <Route path="/login"component={Login} />
            <Route path="/result/:destinationId" component={SearchResult}/>
            <Route path="/mountain/:id" component={MountainDetail}/>
            <Route path="/event/new/" component={CreateEvent} />
            <Route path="/event/mountain/:mountainId" component={ListResult} />
            <Route path="/activity/history"component={History} />
            <Route path="/dataNotFound" component={DataNotFound} />
            <ProtectedRoute exact path="/" component={Home} />
            <ProtectedRoute path="/activity" component={Activity} />
            <ProtectedRoute exact path="/event" component={Event} />
            <ProtectedRoute path="/event/:idEvent" component={EventDetail} />
            <ProtectedRoute exact path="/profile" component={UserProfile} />
            <Route path="/profile/:idUser" component={GuideProfile} />
            <Route component={PageNotFound} />
          </Switch>
          
        </Router>
      </React.Fragment>
    );
  }
}

export default App;

import React from 'react';
import { Link } from 'react-router-dom';

const CardMountain = props => {
  const { _id, name, coverImgPath, location, height} = props.mountain;

  return(
    <React.Fragment>
      <div className="card__item">
        <img 
          src={coverImgPath} 
          alt="sumeru"
          className="card__image card__image--cover"/>
        <div className="card__item__detail">
          <h4> {name} </h4>
          <ul>
            <li className="location"> {location} </li>
            <li className="mdpl"> {height} Mdpl</li>
          </ul>
          <Link to={{ 
            pathname : `/mountain/:${_id}`,
            state: {
              mountain : {...props.mountain}
            }
          }} className="card__link"> Lihat Detail </Link>
        </div>
      </div>
    </React.Fragment>
  );
}


export default CardMountain;
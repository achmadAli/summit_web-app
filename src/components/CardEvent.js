import React from 'react';
import {Link} from 'react-router-dom';
import cardImage from '../assets/images/event_art.png';
import Helper from '../helpers/helper';

const CardEvent = (props) => {
  const {_id, title, leader, startAt, maximumMembers, members, type, price } = props.event;
  return ( 
    <div className="card__item">
      <img 
        src={cardImage} 
        alt="eventImage"
        className="card__image card__image--small"/>
      <div className="card__item__detail">
        <h3> {title} </h3>
        <table>
          <tbody>
            <tr>
              <td> Leader</td>
              <td> : {leader.name}</td>
            </tr>
            <tr>
              <td> Berangkat </td>
              <td> : {Helper.formatDate(startAt)} </td>
            </tr>
            <tr>
              <td> Anggota</td>
              <td> : {members.length} / {maximumMembers}</td>
            </tr>
            <tr>
              <td> Jenis</td>
              <td> : {Helper.changeEventType(type)} {type === 1 ? '' : "| " + price }</td>
            </tr>
          </tbody>
        </table>
        <Link to={{
          pathname : `/event/${_id}`,
          state : {
            event : props.event
          }
        }} className="card__link ml-1"> Lihat Detail </Link>
      </div>
    </div>
   );
}
 
export default CardEvent;
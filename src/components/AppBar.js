import React from 'react';
import hikingLocation from '../assets/images/icons/hikeLocation.svg';

const AppBar = (props) => {

  function setClassModifier(){
    return props.title.toLowerCase() === "activity" ? "history" : "back";
  }
  
  function setVisibility(){
    if (props.destination === undefined)
      return "display-none";
  }

  function topNavigationTrigger(element){
    let option = element.target.className;

    if(option.search("back") > -1){
      props.history.goBack();
    } else {
      props.history.push('/activity/history');
    }    
  }

  return(
    <div className="app-bar">
      <div className="app-bar__title">
        <button onClick={topNavigationTrigger} className={`app-bar__btn-icon app-bar__btn-icon--${setClassModifier()}`} > </button>
        <h3> {props.title} </h3>
      </div>
      <div className={`app-bar__location ${setVisibility()}`}>
        <p> Pendakian </p>
        <img 
          src={hikingLocation} 
          alt="hiking location img"
          width={15}/>
        <button className="btn btn--transparent p-0 ml-1"> Gunung {props.destination}</button>
      </div>
    </div>
  );
}

export default AppBar;
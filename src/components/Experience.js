import React from 'react';
// import dummyBadge from '../assets/images/badges/mountain.svg'; 

const Experience = (props) =>{
  const {image, mountain, year} = props;

  return(
    <div className="user-experience__badges__item">
      <img 
        src={image} 
        alt="dummyBagde" 
        className="img-responsive"/>
      <p> {mountain} </p>
      <p> {year} </p>
    </div>
  );
}

export default Experience;
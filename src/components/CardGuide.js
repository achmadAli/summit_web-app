import React from 'react';
import {Link} from 'react-router-dom';
import cardImage from '../assets/images/guide.svg';

const CardGuide = (props) => {
  console.log(props.guide);
  
  return ( 
    <div className="card__item card__item--vertical">
      <img 
        src={cardImage} 
        alt="eventImage"
        className="img-responsive"/>
      <div className="card__item__detail text-center">
        <h3 className="mt-3"> {props.guide.user.name} </h3>
        <p> Rate : 8/10 </p>
        <Link to={{
          pathname: `/profile/${props.guide.user._id}`,
          state : {
            guide : props.guide
          } 
        }} className="btn btn--small btn--block btn__outline-primary"> Lihat Detail </Link>
      </div>
    </div>
   );
}
 
export default CardGuide;
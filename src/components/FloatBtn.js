import React from 'react';
import addIcon from '../assets/images/icons/add.svg'

const FloatBtn = (props) =>{

  function toCreateEvent(event){
    props.history.push('/event/new')
  }

  return(
    <React.Fragment>
      <button className="btn btn--transparent btn--float" onClick={toCreateEvent}> 
        <img src={addIcon} height="42px" alt=""/>  
      </button>
    </React.Fragment>
  );
}

export default FloatBtn;
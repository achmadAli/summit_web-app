import React, {Component} from 'react';
import search from '../assets/images/icons/search.svg';

class Search extends Component {
  constructor(props){
    super(props);
    this.state ={
      destination : ""
    }

    this.formHandler    = this.formHandler.bind(this);
    this.submitHandler  = this.submitHandler.bind(this);
  }

  formHandler(event){
    event.preventDefault();

    this.setState({
      destination : event.target.value
    });
  }

  submitHandler(event){
    event.preventDefault();

    const request = require('request');
    let option = {
      headers: {'content-type' : 'application/json'},
      uri:     `http://192.168.43.129:3000/mountain?q=Gunung ${this.state.destination}`
    }
    
    request(option, (error, {body, statusCode}) => {
      if(statusCode === 200 && !error && body !== ''){
        let fetchResult = JSON.parse(body);
        let destination = fetchResult.name;
        
        this.props.history.push(`/result/${destination}`, {...fetchResult});
      } else {
        this.props.history.push('/dataNotFound');
      }
    })
  }

  render(){
    return(
      <React.Fragment>
        <form className="form" onSubmit={this.submitHandler}>
          <input 
            type="text" 
            name="search-form" 
            className="form__input form__input--rounded mb-3"
            placeholder="Search another mountain ..."
            onChange={this.formHandler}/>

          <button type="submit" className="btn btn--transparent form__input__search"> <img src={search} alt="search btn"/> </button>
        </form>
      </React.Fragment>
    );
  }
}

export default Search;
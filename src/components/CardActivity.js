import React from 'react';
import User from '../User';
import leaderImage from '../assets/images/dummyPhoto.jpg';

const CardActivity = (props) => {

  function acceptRequest(idActivity){
    const request = require('request');
    const option = {
      headers: {
        'content-Type'  : 'application/json',
        'Authorization' : `Bearer ${User.getToken()}`
      },
      uri:     `http://192.168.43.129:3000/accept/${idActivity}`,
    }

    request.post(option, (error, {body, statusCode})=>{
      if(statusCode === 200){
        props.history.push('events');
      }
    })
    
  }
  return(
       <div className="event__leader__information  mt-4s">
          <img 
            src={leaderImage} 
            alt="leaderImage"
            className="event__leader__image"/>

          <div className="ml-3">
            <h3> Request Menjadi Guide</h3>
            <h4> Achmad Ali Baidlowi</h4>
            <p> 20 tahun </p>
            <p> Sidoarjo </p>
              <div className="display-flex" style={{flexBasis: "100%"}}>
                <button className="btn btn--small btn--primary mr-1" onClick={acceptRequest}> Accept</button>
                <button className="btn btn--small btn--transparent"> Decline </button>
              </div>
          </div>
        </div>
  );
}

export default CardActivity;
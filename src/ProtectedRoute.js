import React from "react";
import { Route, Redirect } from "react-router-dom";
import User from "./User";
import Home from './Screens/Home';

const ProtectedRoute = ({ component: Component, ...rest}) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (User.isAuthenticated()) {
          return <Component {...props} />;
        } else if(!User.isAuthenticated() && Component === Home ){
          return <Redirect to="/start" />;
        } else 
          return <Redirect to="/login" />;
      }}
    />
  );
};


export default ProtectedRoute;
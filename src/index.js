import React from 'react';
import ReactDOM from 'react-dom';
import './assets/scss/bundle.scss';
import App from './App';
import MaintenanceDesktop from './Screens/MaintenanceDesktop';
import * as serviceWorker from './serviceWorker';

if (window.innerWidth < 478){
  ReactDOM.render(<App />, document.getElementById('root'));
} else {
  ReactDOM.render(<MaintenanceDesktop />, document.getElementById('root'));
}
serviceWorker.unregister();
